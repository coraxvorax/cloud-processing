## Configuration ##

Add to /etc/hosts something like:

~~~
192.168.1.1 discovery-service
192.168.1.1 config-service
~~~

## Build and run ##

~~~
docker-compose up
~~~