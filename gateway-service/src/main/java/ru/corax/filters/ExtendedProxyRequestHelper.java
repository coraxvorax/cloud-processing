package ru.corax.filters;

import com.netflix.zuul.context.RequestContext;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.cloud.netflix.zuul.filters.ProxyRequestHelper;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

@Component
@Primary
@Slf4j
public class ExtendedProxyRequestHelper extends ProxyRequestHelper {
	
	private static final String ZUUL_URL_PREFIX = "/zuul";

	@Override
	public String buildZuulRequestURI(HttpServletRequest request) {
		RequestContext context = RequestContext.getCurrentContext();
		String contextURI = (String) context.get("requestURI");
		if (StringUtils.isNotEmpty(contextURI) && !request.getRequestURI().startsWith(ZUUL_URL_PREFIX)) {
			LOG.debug("contextURI is present. Returning the Request URI  : {}",  request.getRequestURI());
			return request.getRequestURI();
		} else {
			LOG.debug("Building Zuul Request using the default method : {}", super.buildZuulRequestURI(request));
			return super.buildZuulRequestURI(request);
		}
	}

}