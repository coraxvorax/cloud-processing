package ru.corax.controller;

import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import ru.corax.model.dto.EventsInfo;
import ru.corax.service.EventReceiver;
import ru.corax.service.EventViewer;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;


@RestController
@RequestMapping("event")
@RequiredArgsConstructor
public class EventController {

	private final EventReceiver eventReceiver;
	private final EventViewer eventViewer;

	@PostMapping("/receive")
	@ApiOperation("Receive events")
	@ResponseStatus(HttpStatus.CREATED)
	public void receive() {
		eventReceiver.receive();
	}

	@GetMapping(value = "/last/{count}/minutes", produces = APPLICATION_JSON_VALUE)
	@ApiOperation("Get events")
	@ResponseStatus(HttpStatus.OK)
	public EventsInfo getEventsCountForLastMinutes(@PathVariable("count") Integer countMinutes) {
		return EventsInfo.of(eventViewer.getEventCountForLastMinutes(countMinutes));
	}

	@GetMapping(value = "/last/{count}/hours", produces = APPLICATION_JSON_VALUE)
	@ApiOperation("Get events")
	@ResponseStatus(HttpStatus.OK)
	public EventsInfo getEventsCountForLastHours(@PathVariable("count") Integer countHours) {
		return EventsInfo.of(eventViewer.getEventCountForLastHours(countHours));
	}

	@GetMapping(value = "/last/{count}/days", produces = APPLICATION_JSON_VALUE)
	@ApiOperation("Get events")
	@ResponseStatus(HttpStatus.OK)
	public EventsInfo getEventsCountForLastDays(@PathVariable("count") Integer countDays) {
		return EventsInfo.of(eventViewer.getEventCountForLastMinutes(countDays));
	}
}