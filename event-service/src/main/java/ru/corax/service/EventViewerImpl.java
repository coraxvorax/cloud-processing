package ru.corax.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.corax.EventTimestamp;
import ru.corax.model.SystemEvent;
import ru.corax.storage.EventStorage;

@Slf4j
@Service
@RequiredArgsConstructor
public class EventViewerImpl implements EventViewer {

	private final EventStorage<SystemEvent> systemEventEventStorage;

	@Override
	public long getEventCountForLastMinutes(Integer countMinutes) {
		return systemEventEventStorage.getCountOfEvents(EventTimestamp.getRangeForLastMinutes(countMinutes));
	}

	@Override
	public long getEventCountForLastHours(Integer countHours) {
		return systemEventEventStorage.getCountOfEvents(EventTimestamp.getRangeForLastHours(countHours));
	}

	@Override
	public long getEventCountForLastDay(Integer countDays) {
		return systemEventEventStorage.getCountOfEvents(EventTimestamp.getRangeForLastDays(countDays));
	}
}
