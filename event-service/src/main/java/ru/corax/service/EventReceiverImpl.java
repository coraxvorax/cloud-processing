package ru.corax.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.corax.model.SystemEvent;
import ru.corax.storage.EventStorage;

import java.util.UUID;

@Slf4j
@Service
@RequiredArgsConstructor
public class EventReceiverImpl implements EventReceiver {

	private final EventStorage<SystemEvent> systemEventEventStorage;

	@Override
	public void receive() {
		final SystemEvent systemEvent = SystemEvent.builder()
				.fotoID(UUID.randomUUID())
				.timestampCreation(System.currentTimeMillis())
				.build();
		LOG.info("Receive new event: {}", systemEvent);
		systemEventEventStorage.save(systemEvent);
	}
}
