package ru.corax.service;

public interface EventViewer {
	long getEventCountForLastMinutes(Integer countMinutes);

	long getEventCountForLastHours(Integer countHours);

	long getEventCountForLastDay(Integer countDays);
}
