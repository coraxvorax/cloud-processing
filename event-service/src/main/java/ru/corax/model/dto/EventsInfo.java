package ru.corax.model.dto;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class EventsInfo {
	private Long countOfEvents;

	public static EventsInfo of(long countOfEvents) {
		return new EventsInfo(countOfEvents);
	}
}
