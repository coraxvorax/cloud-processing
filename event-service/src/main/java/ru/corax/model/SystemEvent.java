package ru.corax.model;

import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

import java.util.UUID;

@Getter
@Builder
@ToString
@RequiredArgsConstructor
public class SystemEvent implements Comparable<SystemEvent> {
	private final UUID fotoID;
	private final Long timestampCreation;

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		SystemEvent fotoEvent = (SystemEvent) o;
		if (!fotoID.equals(fotoEvent.fotoID)) {
			return false;
		}
		return timestampCreation.equals(fotoEvent.timestampCreation);

	}

	@Override
	public int hashCode() {
		int result = fotoID.hashCode();
		result = 31 * result + timestampCreation.hashCode();
		return result;
	}

	@Override
	public int compareTo(SystemEvent o) {
		if (this.timestampCreation < o.timestampCreation) {
			return -1;
		} else if (this.timestampCreation.equals(o.timestampCreation)) {
			return 0;
		} else {
			return 1;
		}
	}
}