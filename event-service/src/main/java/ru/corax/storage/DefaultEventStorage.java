package ru.corax.storage;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import ru.corax.EventTimestamp;
import ru.corax.model.SystemEvent;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ConcurrentSkipListMap;

@Slf4j
@Component
@RequiredArgsConstructor
public class DefaultEventStorage implements EventStorage<SystemEvent> {

	private final ConcurrentSkipListMap<Long, Queue<SystemEvent>> eventsCache;

	@Override
	public void save(SystemEvent event) {
		final Long timestamp = event.getTimestampCreation();
		final Long shiftKey = getShiftKeyOf(timestamp);

		eventsCache.computeIfPresent(shiftKey, (k, q) -> {
			q.offer(event);
			return q;
		});
		eventsCache.computeIfAbsent(shiftKey, (k) -> {
			final Queue<SystemEvent> queue = new ConcurrentLinkedQueue<>();
			queue.offer(event);
			return queue;
		});
	}

	private Long getShiftKeyOf(Long timestamp) {
		return EventTimestamp.createShiftKeyOf(timestamp);
	}

	@Override
	public long getCountOfEvents(EventTimestamp.Range range) {
		return eventsCache.subMap(range.getStart(), true, range.getEnd(), true)
				.entrySet().stream().mapToInt(e -> e.getValue().size()).sum();
	}
}
