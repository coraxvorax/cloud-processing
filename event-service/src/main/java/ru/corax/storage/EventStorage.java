package ru.corax.storage;

import ru.corax.EventTimestamp;

public interface EventStorage<T> {

	void save(T event);

	long getCountOfEvents(EventTimestamp.Range range);
}
