package ru.corax.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.corax.model.SystemEvent;

import java.util.Queue;
import java.util.concurrent.ConcurrentSkipListMap;

@Configuration
public class MainConfig {

	@Bean
	public ConcurrentSkipListMap<Long, Queue<SystemEvent>> cache() {
		return new ConcurrentSkipListMap<>();
	}
}
