package ru.corax;

import static java.util.concurrent.TimeUnit.DAYS;
import static java.util.concurrent.TimeUnit.HOURS;
import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static java.util.concurrent.TimeUnit.MINUTES;

public class EventTimestamp {

    public static EventTimestamp.Range getRangeForLastMinutes(int countOfMinutes) {
        final long currentTimestamp = System.currentTimeMillis();
        return new ImmutableRange(
                currentTimestamp - MILLISECONDS.convert(countOfMinutes, MINUTES), currentTimestamp
        );
    }

    public static EventTimestamp.Range getRangeForLastHours(int countOfHours) {
        final long currentTimestamp = System.currentTimeMillis();
        return new ImmutableRange(
                currentTimestamp - MILLISECONDS.convert(countOfHours, HOURS), System.currentTimeMillis()
        );
    }

    public static EventTimestamp.Range getRangeForLastDays(int countOfDays) {
        final long currentTimestamp = System.currentTimeMillis();
        return new ImmutableRange(
                currentTimestamp - MILLISECONDS.convert(countOfDays, DAYS), System.currentTimeMillis()
        );
    }

    public static Long createShiftKeyOf(Long timestamp) {
        return (timestamp / 1000) * 1000;
    }

    public interface Range {
        Long getStart();

        Long getEnd();
    }

    private static class ImmutableRange implements Range {
        private final Long start;
        private final Long end;

        private ImmutableRange(Long start, Long end) {
            this.start = start;
            this.end = end;
        }

        @Override
        public Long getStart() {
            return start;
        }

        @Override
        public Long getEnd() {
            return end;
        }
    }
}
